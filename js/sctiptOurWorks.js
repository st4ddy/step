let data = Array.from(document.querySelectorAll('.workExemple .workItems')),
    step = 12,
    item = 0;

data.slice(step).forEach(e => e.style.display = 'none');
item += step;

document.querySelector('#more').addEventListener('click', function(e){
  let tmp = data.slice(item, item + step);
  tmp.forEach(e => e.style.display = 'inline-block');
  item += step;
  
  if(tmp.length < 24)
    this.remove();
});


